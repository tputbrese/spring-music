#a valid file type variable based on the structure in kubeconfig-data.yaml should exist 
#cluster_context_spec should be specifed as the name of the file type variable

if test -f "ci_kubeconfig"; then
  echo "ci_kubeconfig exists."
else
  cluster_context_file="${!cluster_context_spec}"
  mv $cluster_context_file ${cluster_context_file}.yaml
  ytt -f kube/kubeconfig.yaml -f ${cluster_context_file}.yaml > ci_kubeconfig
fi

export KUBECONFIG=ci_kubeconfig
