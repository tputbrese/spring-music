#!/bin/bash

. kube/kubeconfig.sh

REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"

ytt -f kube/deployment.yaml -f values.yaml -v root_dns_name=${ingress_domain} -v registry=${REGISTRY_IMAGE} -v namespace=$namespace -v image_name=${CI_COMMIT_BRANCH} -v tag=latest -v app_name=$CI_PROJECT_NAME |  kubectl --kubeconfig=$kube_config -n $namespace apply -f-
